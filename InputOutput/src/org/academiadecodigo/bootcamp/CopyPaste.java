package org.academiadecodigo.bootcamp;

import java.io.*;

public class CopyPaste {

    public void start() throws IOException {

        FileInputStream inputStream = new FileInputStream("Teste1.rtf");

        byte[] buffer = new byte[inputStream.available()];
        inputStream.read(buffer, 0, buffer.length);
        inputStream.close();
        FileOutputStream outputStream = new FileOutputStream("vazio.rtf");
        outputStream.write(buffer);
        outputStream.close();
    }
}
