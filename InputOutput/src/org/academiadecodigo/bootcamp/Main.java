package org.academiadecodigo.bootcamp;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        CopyPaste copyPaste = new CopyPaste();
        copyPaste.start();
    }
}
