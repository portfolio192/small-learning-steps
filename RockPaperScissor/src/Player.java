public class Player {

    private String name;
    private String choice;

    public Player(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

   public String choice() {
        return choice = RockPaperScissor.giveMeRandom();
    }
}
