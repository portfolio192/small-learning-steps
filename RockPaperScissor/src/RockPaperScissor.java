public enum RockPaperScissor {
    ROCK("Rock"),
    PAPER("Paper"),
    SCISSOR("Scissor");

    private String symbol;

    RockPaperScissor(String symbol) {
        this.symbol = symbol;

    }
    public String getSymbol() {
        return this.symbol;
    }
    public static String giveMeRandom() {
            return values()[(int) (Math.random() * values().length)].getSymbol();
        }
    }

