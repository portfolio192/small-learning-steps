package org.academiadecodigo.bootcamp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) throws IOException {

        int portNumber = 9999;

        ServerSocket serverSocket = new ServerSocket(portNumber);
        Socket clientSocket = serverSocket.accept();
            while (true) {
                BufferedReader receive = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                String receiveContent = receive.readLine();
                System.out.println(receiveContent);

                PrintWriter sendContent = new PrintWriter(clientSocket.getOutputStream(), true);
                sendContent.println(getMessage());
            }
    }

    private static String getMessage() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("=============================");
        return reader.readLine();
    }
}
