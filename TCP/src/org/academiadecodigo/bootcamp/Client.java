package org.academiadecodigo.bootcamp;

import java.io.*;
import java.net.Socket;

public class Client {

    public static void main(String[] args) throws IOException {

        Socket clientSocket = new Socket("localhost", 9999);
        while (true) {
        PrintWriter sendContent = new PrintWriter(clientSocket.getOutputStream(), true);
        sendContent.println(getMessage());

        BufferedReader receive = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        String receiveContent = receive.readLine().toUpperCase();
        System.out.println(receiveContent);
        }
    }

    private static String getMessage() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("=============================");
        return reader.readLine();
    }
}
