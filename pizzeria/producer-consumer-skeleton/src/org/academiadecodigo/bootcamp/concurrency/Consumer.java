package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;
import org.academiadecodigo.bootcamp.concurrency.bqueue.Pizza;

/**
 * Consumer of integers from a blocking queue
 */
public class Consumer implements Runnable {

    private final BQueue<Pizza> queue;
    private int elementNum;

    /**
     * @param queue the blocking queue to consume elements from
     * @param elementNum the number of elements to consume
     */
    public Consumer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.elementNum = elementNum;
    }

    @Override
    public void run() {
            while(elementNum > 0) {
                try {
                    Thread.sleep((int) ((Math.random() * (1500 - 100)) + 100));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                queue.poll();
                elementNum--;
            }
        if(elementNum == 0) {
            System.out.println("No more pizzas today for consumer " + Thread.currentThread().getName());
            Thread.currentThread().interrupt();
        }
    }
}
