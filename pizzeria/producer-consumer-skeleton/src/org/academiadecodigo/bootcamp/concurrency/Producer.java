package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;
import org.academiadecodigo.bootcamp.concurrency.bqueue.Pizza;

/**
 * Produces and stores integers into a blocking queue
 */
public class Producer implements Runnable {

    private final BQueue<Pizza> queue;
    private int elementNum;

    /**
     * @param queue      the blocking queue to add elements to
     * @param elementNum the number of elements to produce
     */
    public Producer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.elementNum = elementNum;
        BQueue.numberOfColleagues++;
    }

    @Override
    public void run() {
        while (elementNum > 0) {
            try {
                Thread.sleep((int) ((Math.random() * (1500 - 100)) + 100));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            queue.offer(new Pizza());
            elementNum--;
        }
        if (elementNum == 0) {
            System.out.println("Producer " + Thread.currentThread().getName() + " can't make no more pizzas for today");
            BQueue.numberOfColleaguesThatFinishedTheirJob += 1;
            if (BQueue.numberOfColleaguesThatFinishedTheirJob >= BQueue.numberOfColleagues) {
                System.out.println("All the producers are out! The shop is closed");
                System.exit(0);
            }
        }
    }
}