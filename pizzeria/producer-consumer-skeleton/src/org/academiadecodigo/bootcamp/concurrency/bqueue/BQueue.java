package org.academiadecodigo.bootcamp.concurrency.bqueue;

import java.util.LinkedList;

/**
 * Blocking Queue
 *
 * @param <T> the type of elements stored by this queue
 */
public class BQueue<T> {

    private LinkedList<T> list;
    private int limit;

    public static int numberOfColleaguesThatFinishedTheirJob = 0;
    public static int numberOfColleagues = 0;

    /**
     * Constructs a new queue with a maximum size
     *
     * @param limit the queue size
     */
    public BQueue(int limit) {
        this.list = new LinkedList<>();
        this.limit = limit;
    }

    /**
     * Inserts the specified element into the queue
     * Blocking operation if the queue is full
     *
     * @param data the data to add to the queue
     */
    public synchronized void offer(T data) {
        while (list.size() == limit) {
            System.out.println("Producer " + Thread.currentThread().getName() + " there's 5 pizzas in the queue");
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        if (list.size() < limit) {
            list.offer(data);
            notify();
            System.out.println("Producer " + Thread.currentThread().getName() + " made 1 pizza of " + data);
        }


    }

    /**
     * Retrieves and removes data from the head of the queue
     * Blocking operation if the queue is empty
     *
     * @return the data from the head of the queue
     */
    public synchronized T poll() {
        while (getSize() == 0) {
            System.out.println("Consumer " + Thread.currentThread().getName() + " ,No pizzas in the table");
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        T counter = list.poll();
        notify();
        System.out.println("Consumer " + Thread.currentThread().getName() + " ate 1 pizza of " + counter);

        return counter;
    }


    /**
     * Gets the number of elements in the queue
     *
     * @return the number of elements
     */
    public synchronized int getSize() {
        return list.size();
    }

    /**
     * Gets the maximum number of elements that can be present in the queue
     *
     * @return the maximum number of elements
     */
    public int getLimit() {
        return limit;
    }

}
