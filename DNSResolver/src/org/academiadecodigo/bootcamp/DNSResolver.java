package org.academiadecodigo.bootcamp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class DNSResolver {

    public static void main(String[] args) {
        try {
            String host = getHost();
            InetAddress address = InetAddress.getByName(host);


            System.out.println("Testing reachability...");
            System.out.println((address.isReachable(1000) ? "OK" : "fail"));

        } catch (UnknownHostException ex) {
            System.out.println("Invalid host name");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static String getHost() throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Hostname? ");
        return reader.readLine();
    }
}
