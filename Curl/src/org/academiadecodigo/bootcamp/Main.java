package org.academiadecodigo.bootcamp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Main {

    public static void main(String[] args) throws IOException {

        URL connection = new URL("https://www.stackoverflow.com");

        BufferedReader input = new BufferedReader(new InputStreamReader(connection.openStream()));
        while(input.readLine() != null) {
            String content = input.readLine();
            System.out.println(content);
        }
    }
}

