package org.academiadecodigo.bootcamp;

import java.util.Iterator;
import java.util.PriorityQueue;

public class TodoList implements Iterable {

    private PriorityQueue <TodoItem> todoItem;

    PriorityQueue <TodoItem> queue = new <TodoItem> PriorityQueue ();

    public void add(TodoItem todoItem) {
        queue.add(todoItem);
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public TodoItem remove() {
        return queue.remove();
    }

    @Override
    public Iterator iterator() {
        return todoItem.iterator();
    }
}
