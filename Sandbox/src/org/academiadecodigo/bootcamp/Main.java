package org.academiadecodigo.bootcamp;



public class Main {

    public static void main(String[] args) {
        TodoList todoList = new TodoList();
        todoList.add(new TodoItem(Importance.MEDIUM, 1, "Get wasted at the next «Code Breako,"));
        todoList.add(new TodoItem(Importance.LOW, 1, "Eat a bifana."));
        todoList.add(new TodoItem(Importance.HIGH, 1, "Hear Isabela saying \"Peaky Fooking Blinders\"."));
        todoList.add(new TodoItem(Importance.LOW, 2, "Do today's punishment."));
        todoList.add(new TodoItem(Importance.MEDIUM, 2, "Get some rest."));
        todoList.add(new TodoItem(Importance.HIGH, 2, "Cry because my code doesn't compile."));


        while (!todoList.isEmpty()) {
            System.out.println(todoList.remove());
        }
    }
}