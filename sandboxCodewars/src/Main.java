import java.util.Arrays;
public class Main {

    public static void main(String[] args) {
        System.out.println(stripComments("apples, pears # and bananas\ngrapes\nbananas !apples", new String[]{"#", "!"}));
    }

    public static String stripComments(String text, String[] commentSymbols) {


        String[] phrases = text.split("\n");

        for (int j = 0; j < phrases.length; j++) {

            String[] letters = phrases[j].split("");

            for (int i = 0; i < letters.length; i++) {

                if (Arrays.asList(commentSymbols).contains(letters[i])) {
                    phrases[j] = phrases[j].substring(0,i).replaceAll("[ \t]+$", "");
                }
            }
        }
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < phrases.length; i++) {
            stringBuilder.append(phrases[i]);
            if(i != phrases.length - 1){
                stringBuilder.append("\n");
                System.out.println(phrases[i].charAt(phrases[i].length() - 1));
            }
        }

        return stringBuilder.toString();
    }



}
