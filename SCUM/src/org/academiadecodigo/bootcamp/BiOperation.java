package org.academiadecodigo.bootcamp;

@FunctionalInterface
public interface BiOperation<T> {

    T doIt(T value1, T value2);

}
