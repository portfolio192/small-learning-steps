package org.academiadecodigo.bootcamp;

public class Machine {

    public String executeString(String placeHolder) {
        MonoOperation <String> monoOperation = value -> value + value;
        return monoOperation.doIt(placeHolder);
    }

    public Integer executeInteger(Integer placeHolder1, Integer placeHolder2) {
        BiOperation <Integer> biOperation = (value1, value2) -> value1 + value2;
        return biOperation.doIt(placeHolder1,placeHolder2);
    }
}


