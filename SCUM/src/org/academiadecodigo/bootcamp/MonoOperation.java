package org.academiadecodigo.bootcamp;

@FunctionalInterface
public interface MonoOperation<T> {

    T doIt(T value);

}
