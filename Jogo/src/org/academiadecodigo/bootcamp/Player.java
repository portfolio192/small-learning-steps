package org.academiadecodigo.bootcamp;

public class Player {

    public String name;
    private int number;

    public Player(String name) {
        this.name = name;
        this.number = (int) Math.floor(Math.random() * 10);

    }

    public int generateNumber() {

        this.number = (int) (Math.floor(Math.random() * 10));
        return number;
    }

    public int getNumber() {
        return number;
    }
}
