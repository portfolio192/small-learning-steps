package org.academiadecodigo.bootcamp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;

public class UDPServer {

    public static void main(String[] args) throws IOException {

        int portNrServer = 8888;

        byte[] recBuffer = new byte[1024];

        DatagramSocket socket = new DatagramSocket(portNrServer);

        DatagramPacket receivePacket = new DatagramPacket(recBuffer, recBuffer.length);

        socket.receive(receivePacket);
        String contentReceived = new String (receivePacket.getData());
        System.out.println(contentReceived);
        String contentReceivedUpper = contentReceived.toUpperCase();
        byte[] sendBuffer = contentReceivedUpper.getBytes();

        DatagramPacket sendPacket = new DatagramPacket(sendBuffer,
                sendBuffer.length, receivePacket.getAddress(), receivePacket.getPort());
        socket.send(sendPacket);
    }
}
