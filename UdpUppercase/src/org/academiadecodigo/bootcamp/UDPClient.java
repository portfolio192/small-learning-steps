package org.academiadecodigo.bootcamp;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.*;

public class UDPClient {

    public static void main(String[] args) throws IOException {

        int portNrClient = 9999;

        byte[] sendBuffer = getMessage().getBytes();
        byte[] recBuffer = new byte[1024];

        DatagramSocket socket;
        socket = new DatagramSocket(portNrClient);

        DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, InetAddress.getByName("localhost"), 8888);
        socket.send(sendPacket);

        DatagramPacket receivePacket = new DatagramPacket(recBuffer, recBuffer.length);

        socket.receive(receivePacket);
        String contentReceived = new String (receivePacket.getData());
        System.out.println(contentReceived);
    }

    private static String getMessage() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Message? ");
        return reader.readLine();
    }
}