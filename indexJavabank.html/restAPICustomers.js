$(document).ready(function () {
    doGet();
    setupButtons();
})

function setupButtons() {
    $("#buttonAdd").on("click", doPost);
    $("#buttonUpdate").on("click", doUpdate);
    $("#buttonReset").on("click", resetCustomer);
}

function errorCallback(request, status, error) {
    console.log(request)
    console.log(status)
    console.log(error)
}

function doGet() {
    $("#user_table").empty();
    resetCustomer();

    $.ajax({
        url: 'http://localhost:8080/javabank5/api/customer/',
        type: 'GET',
        async: true,
        success: loadCustomers,
        error: errorCallback
    });
}

function doPost() {

    $("#added").show();
    $("#edited").hide();
    $("#deleted").hide();

    $.ajax({
        url: 'http://localhost:8080/javabank5/api/customer/',
        type: 'POST',
        data: JSON.stringify({
            firstName: $('#validationFirstName').val(),
            lastName: $('#validationLastName').val(),
            email: $('#validationEmail').val(),
            phone: $('#validationPhone').val(),
        }),
        async: true,
        contentType: 'application/json',
        success: doGet,
        error: errorCallback
    });
}

function doDelete(event) {

    $("#deleted").show();

    var id = event.target.id.split("-")[2];
    $.ajax({
        url: 'http://localhost:8080/javabank5/api/customer/' + id,
        type: 'DELETE',
        async: true,
        success: doGet,
        error: errorCallback
    });
}

function doEdit(event) {

    var id = event.target.id.split("-")[2];
    $.ajax({
        url: 'http://localhost:8080/javabank5/api/customer/' + id,
        type: 'GET',
        async: true,
        success: function(response){ 
            $('#validationFirstName').val(response.firstName),
            $('#validationLastName').val(response.lastName),
            $('#validationEmail').val(response.email),
            $('#validationPhone').val(response.phone),
            $('#customerId').val(response.id)},
        error: errorCallback
    });
}

function doUpdate() {

    $("#edited").show();
    $("#added").hide();
    $("#deleted").hide();

    var id = $('#customerId').val();
    console.log(id);

    $.ajax({
        url: 'http://localhost:8080/javabank5/api/customer/'+ id,
        type: 'PUT',
        data: JSON.stringify({
            firstName: $('#validationFirstName').val(),
            lastName: $('#validationLastName').val(),
            email: $('#validationEmail').val(),
            phone: $('#validationPhone').val(),
        }),
        async: true,
        contentType: 'application/json',
        success: doGet,
        error: errorCallback
    });
}



function loadCustomers(response) {
   
    var table = $('#user_table');

    response.forEach(function (element) {
        var htmlStr = "<tr id=customer-data><td>" + element.firstName +
            "</td>" + "<td>" + element.lastName +
            "</td>" + "<td>" + element.email +
            "</td>" + "<td>" + element.phone +
            "</td>" +
            '<td><button type="button" id="edit-btn-' + element.id +
            '" class="edit-btn btn btn-success">edit</button></td>'
            + '<td><button type="button" id="remove-btn-' + element.id +
            '" class="remove-btn btn btn-danger">delete</button></td></tr>';
        $(htmlStr).appendTo(table);

        $("#edit-btn-" + element.id).on("click", doEdit);
        $("#remove-btn-" + element.id).on("click", doDelete);

    });
}

function resetCustomer() {
    $('.form-control').val('')
}