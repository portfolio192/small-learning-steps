window.onload = function() {

var customerData = [
    {"id":1,"firstName":"Rui","lastName":"Ferrão","email":"rui@gmail.com","phone":"777888"},
    {"id":2,"firstName":"Sergio","lastName":"Gouveia","email":"sergio@gmail.com","phone":"777999"},
    {"id":3,"firstName":"Bruno","lastName":"Ferreira","email":"bruno@gmail.com","phone":"777666"},
    {"id":4,"firstName":"Rodolfo","lastName":"Matos","email":"rodolfo@gmail.com","phone":"777333"}
];

var usersTable = document.getElementById('user_table');


customerData.forEach(function(element) {
    var row = usersTable.insertRow(-1);
    var btn = document.getElementById("user_table");
    var htmlStr = "<tr><td>" + element.firstName + "</td></tr>" + "<tr><td>" + element.lastName + "</td></tr>" + "<tr><td>" + 
    element.email + "</td></tr>" + "<tr><td>" + element.phone + "</td></tr>" + "<td><a class='btn btn-warning'>Edit</a></td>" +
    "<td><a class='btn btn-danger'>Delete</a></td>"
    row.innerHTML = htmlStr
});

}