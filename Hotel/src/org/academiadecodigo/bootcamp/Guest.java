package org.academiadecodigo.bootcamp;

public class Guest {
    private String name;
    private Hotel hotel;
    private int roomNumber;

    public Guest(String name, Hotel hotel) {
        this.name = name;
        this.hotel = hotel;
    }

    public String getName() {
        return this.name;
    }

    public void checkIn() {
        hotel.checkIn();
    }

    public void checkOut(int roomNumber) {
        hotel.checkOut(hotel.checkRoomNumber());
    }

    public void checkOccupancy() {
        hotel.checkOccupancy();
    }
}
