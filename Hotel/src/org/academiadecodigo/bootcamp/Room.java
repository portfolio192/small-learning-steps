package org.academiadecodigo.bootcamp;

public class Room {

    private int roomNumber;
    private boolean occupancy;

    public Room(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getRoomNumber() {
        return this.roomNumber;
    }

    public boolean getOccupancy() {
        return this.occupancy;
    }

    public void occupyRoom() {
        occupancy = true;
    }

    public void desoccupyRoom() {
        occupancy = false;
    }
}
