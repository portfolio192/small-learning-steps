package org.academiadecodigo.bootcamp;

public class Hotel {

    private String designation;
    private Room[] room;
    private Room freedRoom;
    private Room occupiedRoom;

    public Hotel(String designation,int numberOfRooms) {
        this.designation = designation;
        this.room = new Room[numberOfRooms];
        fillRooms();
    }

    public String getDesignation() {
        return this.designation;
    }

    public Room[] getRoom(boolean occupancy) {
        return this.room;
    }

    public int checkIn() {
        if (checkOccupancy() == true) {
            freedRoom.occupyRoom();
        } return freedRoom.getRoomNumber();
    }

    public void checkOut(int roomNumber) {
        checkRoomNumber(roomNumber);
        occupiedRoom.desoccupyRoom();
        System.out.println("Thank you and see you next time!!!");
    }

    public boolean checkOccupancy() {
        for (int i = 0; i < room.length; i++) {
            if (room[i].getOccupancy() == false) {
                freedRoom = room[i];
                System.out.println(room[i].getRoomNumber() + " room reserved");
                return true;
            }
        }
        System.out.println("You have no rooms available");
        return false;
    }

    public void checkRoomNumber(int roomNumber) {
        for (int i = 0; i < room.length; i++) {
             if (room[i].getRoomNumber() == roomNumber) {
                 occupiedRoom = room[i];
                 return;
             }
        }
    }

    public void fillRooms() {
        for (int i = 0; i < room.length; i++) {
            room[i] = new Room(i + 1);
        }
    }
}