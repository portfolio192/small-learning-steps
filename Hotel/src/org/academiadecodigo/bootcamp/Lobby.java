package org.academiadecodigo.bootcamp;

public class Lobby {

    public static void main(String[] args) {
        Hotel hotel = new Hotel("Ritz do Aleixo", 4);
        Guest guest1 = new Guest("Valter", hotel);
        Guest guest2 = new Guest("Josefina", hotel);
        Guest guest3 = new Guest("Violeta", hotel);
        Guest guest4 = new Guest("Pauleta", hotel);
        Guest guest5 = new Guest("Saúl", hotel);

        hotel.checkOccupancy();
        guest1.checkIn();
        guest2.checkIn();
        guest3.checkIn();
        guest1.checkOut();


    }
}
