package org.academiadecodigo.bootcamp;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    public static final int PORT = 8080;
    private Socket clientSocket;
    private ServerSocket socket = null;
    private CopyOnWriteArrayList<ServerWorker> list = new CopyOnWriteArrayList<>();


    public static void main(String[] args) throws IOException {
        Server server = new Server();
        server.creator();
    }

    private void creator() throws IOException {

        socket = new ServerSocket(PORT);
        ExecutorService threadCreator = Executors.newCachedThreadPool();

        while (true) {
            ServerWorker serverWorker = new ServerWorker(socket.accept());
            threadCreator.submit(serverWorker);
            list.add(serverWorker);
            broadcast();
        }
    }

    private void broadcast() throws IOException {

        for (ServerWorker serverWorker : list) {
            serverWorker.sendMessage();
        }

    }

}


