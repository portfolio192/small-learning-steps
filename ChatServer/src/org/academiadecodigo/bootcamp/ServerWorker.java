package org.academiadecodigo.bootcamp;

import java.io.*;
import java.net.Socket;

public class ServerWorker implements Runnable {

    private PrintWriter sendContent;
    private BufferedReader receive;
    private Socket clientSocket;

    public ServerWorker(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        while (true) {
            try {
                receiveMessage();
                sendMessage();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void sendMessage() throws IOException {
        sendContent = new PrintWriter(clientSocket.getOutputStream(), true);
        sendContent.println(getMessage());
    }

    public void receiveMessage() throws IOException {
        receive = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        String receiveContent = receive.readLine();
        System.out.println(receiveContent);
    }

    private static String getMessage() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("=============================");
        return reader.readLine();
    }
}

