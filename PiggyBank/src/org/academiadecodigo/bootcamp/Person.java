package org.academiadecodigo.bootcamp;

public class Person {

    private PiggyBank piggyBank = new PiggyBank(100);
    private Wallet wallet = new Wallet(0);
    private LemonStand lemonStand = new LemonStand(0);

        public PiggyBank getPiggyBank() {
            return piggyBank;
        }
        public void saveToPiggy (int money){
            piggyBank.deposit(money);
            wallet.withdraw(money);
        }

        public void spendSavingsInPiggy (int money) {
            piggyBank.withdraw(money);
            wallet.deposit(money);
        }

        public void checkBalanceInPiggy () {
            piggyBank.checkBalance();
        }
        public Wallet getWallet() {
            return wallet;
        }
        public void saveInWallet (int money){
        wallet.deposit(money);
        }

        public void spendInWallet (int money) {
        wallet.withdraw(money);
        }

        public void checkBalanceWallet () {
        wallet.checkBalance();
        }

        public void buyWater (int unit) {
            Supplier.WATER.buyWater(unit);
                if (wallet.getBalance() <= 0) {
                    System.out.println("You lack funds for that in your wallet!!!");
                    return;
                    }
                    spendInWallet(unit);
                    lemonStand.addWaterToStock(unit);
            }

            public void buyLemons ( int unit) {
                Supplier.LEMON.buyLemons(unit);
                if (wallet.getBalance() <= 0) {
                    System.out.println("You lack funds for that in your wallet!!!");
                    return;
                    }
                    spendInWallet(unit);
                    lemonStand.addLemonsToStock(unit);
            }

            public void checkStock () {
                lemonStand.checkStock();
            }

            public void lemonade (int unit) {
                lemonStand.makeLemonade(unit);
            }

            public void sellLemonade (int unit) {
                if (lemonStand.sellLemonade(unit)) {
                    saveInWallet(unit * 3);
                }
            }
        }