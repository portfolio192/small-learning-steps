package org.academiadecodigo.bootcamp;

public class Main {
    public static void main(String[] args) {

        Person person = new Person();

        person.checkBalanceInPiggy();
        person.spendSavingsInPiggy(30);
        person.checkBalanceWallet();
        person.buyWater(10);
        person.buyLemons(10);
        person.checkBalanceWallet();
        person.lemonade(10);
        person.checkBalanceWallet();
        person.checkStock();
        person.sellLemonade(10);
        person.checkBalanceWallet();
        person.saveToPiggy(40);
        person.checkBalanceInPiggy();
    }

}
