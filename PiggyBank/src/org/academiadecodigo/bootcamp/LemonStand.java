package org.academiadecodigo.bootcamp;

public class LemonStand {

    private int stock;

    int water = 0;
    int lemons = 0;

    public LemonStand (int unit) {
        this.stock = unit;
    }

    public int getStock() {
        return this.stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void addWaterToStock (int unit) {
        water += unit;
    }

    public void addLemonsToStock (int unit) {
        lemons += unit;
    }

    public void checkStock () {
        System.out.println("Your water stock is: " + water);
        System.out.println("Your lemons stock is: " + lemons);
        System.out.println("Your lemonade stock is: " + stock);
    }

    public void makeLemonade (int unit) {
        if (water < unit || lemons < unit) {
            System.out.println("You only have " + water + " waters and " + lemons + " lemons");
            return;
        }
        this.stock += unit;
        water -= unit;
        lemons -= unit;
        System.out.println("You made new lemonade(s) " + unit);
    }

    public boolean sellLemonade (int unit) {
        if (stock < unit) {
            System.out.println("I only have " + stock + " lemonades");
            return false;
        }
        System.out.println("You've sold " + unit + " lemonade(s)");
        stock -= unit;
        return true;
    }

}
