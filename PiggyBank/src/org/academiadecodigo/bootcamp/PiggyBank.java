package org.academiadecodigo.bootcamp;

public class PiggyBank {
    private int balance;
    private PiggyBank piggyBank;

    public PiggyBank (int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return this.balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void deposit(int money) {
        if (money <= 0) {
            System.out.println("Stop kidding with me");
            return;
        }
        setBalance(getBalance() + money);
        System.out.println("You've added " + money + " to your piggybank");
    }

    public void withdraw(int money) {
        if (money <= 0) {
            System.out.println("Stop kidding with me");
            return;
        }
        if (getBalance() < money) {
            System.out.println("You lack funds for that in your piggybank!!!");
            return;
        }
        setBalance(getBalance() - money);
        System.out.println("You've withdrawn " + money + " from your piggybank");
        }
        public void checkBalance() {
        System.out.println("Your balance is now " + getBalance() + " on your piggybank");
        }
}
