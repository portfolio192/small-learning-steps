package org.academiadecodigo.bootcamp;

public enum Supplier {

    WATER(1,100),
    LEMON(1, 100);

        private int price;
        private int stock;

        Supplier(int price, int stock) {
            this.price = price;
            this.stock = stock;
        }

        public int getPrice() {
            return this.price;
        }

        public int getStock() {
            return this.stock;
        }

        public void setStock(int stock) {
            this.stock = stock;
        }

        public void buyWater(int stock) {
            if (stock < 0) {
                System.out.println("Out of stock");
                return;
            }
            WATER.stock -= stock;
        }

        public void buyLemons(int stock) {
            if (stock < 0) {
                System.out.println("Out of stock");
                return;
            }
            LEMON.stock -= stock;
        }
}