package org.academiadecodigo.bootcamp;

public class Wallet {

    private int balance;
    private Wallet wallet;

    public Wallet(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return this.balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void deposit(int money) {
        if (money <= 0) {
            System.out.println("Stop kidding with me");
            return;
        }
        setBalance(getBalance() + money);
        System.out.println("You've added " + money + " to your wallet");
    }

    public void withdraw(int money) {
        if (money <= 0) {
            System.out.println("Stop kidding with me");
            return;
        }
        if (getBalance() < money) {
            System.out.println("You lack funds for that in your wallet!!!");
            return;
        }
        setBalance(getBalance() - money);
        System.out.println("You've withdrawn " + money + " from your wallet");
    }

        public void checkBalance() {
        System.out.println("You have " + getBalance() + " on your wallet");
    }
}