package org.academiadecodigo.bootcamp;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {

    public static void main(String[] args) throws IOException {

        File indexFile = new File("www/test/index.html");
        BufferedReader sendIndex = new BufferedReader(new FileReader(indexFile));
        File logoFile = new File("www/logo.png");
        BufferedReader sendLogo = new BufferedReader(new FileReader(logoFile));

        ServerSocket serverSocket = new ServerSocket(8080);
        Socket clientSocket = serverSocket.accept();
        BufferedReader receive = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        String receiveContent = receive.readLine();
        System.out.println(receiveContent);

        PrintWriter sendContent = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);

        if (receiveContent.contains("GET")) {
            sendContent.println("HTTP/1.0 200 Document Follows \r\n" +
                    "Content-Type: text/html; charset=UTF-8 \r\n" +
                    "Content-Length: " + indexFile.length() + " \r\n" +
                    "\r\n\r\n");
            sendContent.flush();

            String index = "";
            while ((index = sendIndex.readLine()) != null) {
                sendContent.write(index);
                sendContent.flush();
            }
        }

        if (receiveContent.contains("logo.png")) {
            sendContent.println("HTTP/1.0 200 Document Follows\r\n" +
                    "Content-Type: image/" + logoFile + "\r\n" +
            "Content-Length: " + logoFile.length() + "\r\n\r\n");
            sendContent.flush();
            String logo = "";
            while ((logo = sendLogo.readLine()) != null) {
                sendContent.write(logo);
                sendContent.flush();
            }
        }

    }
}
