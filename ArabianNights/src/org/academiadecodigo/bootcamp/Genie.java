package org.academiadecodigo.bootcamp;

public class Genie {

    private int maxWishes;
    private int remainingWishes;
    private boolean demon;

    public Genie(int maxWishes, boolean demon) {
        this.maxWishes = maxWishes;
        this.remainingWishes = maxWishes;
        this.demon = demon;

    }

    public int getMaxWishes() {
        return maxWishes;
    }

    public int getRemainingWishes() {
        return remainingWishes;
    }

    public void grantWishes() {
        remainingWishes--;
        System.out.println("Your wish has been granted!");
        System.out.println("========================================");
    }

    public void recycleDemon() {
        if (demon) {
            System.out.println("You've recycled the Demon");
            System.out.println("========================================");
        } else {
            System.out.println("You can't be recycled");
            System.out.println("========================================");
        }
    }
}
