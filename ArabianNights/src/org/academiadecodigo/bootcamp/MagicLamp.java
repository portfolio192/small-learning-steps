package org.academiadecodigo.bootcamp;

public class MagicLamp {

    private int maxGeniesCapacity;
    private int remainingGenies;
    private int recharges;
    private int maxWishes;
    private int rubs;

    public MagicLamp(int maxGeniesCapacity, int maxWishes) {
        this.maxGeniesCapacity = maxGeniesCapacity;
        this.remainingGenies = maxGeniesCapacity;
        this.maxWishes = maxWishes;
    }

    public int getMaxGeniesCapacity() {
        return maxGeniesCapacity;
    }

    public int getRemainingGenies() {
        return remainingGenies;
    }

    public int getRecharges() {
        return recharges;
    }

    public Genie rubLamp() {
        if (rubs % 2 == 0 && remainingGenies > 0) {
            Genie genie = new FriendlyGenie(maxWishes);
            System.out.println("I'm the Friendly Genie");
            System.out.println("========================================");
            rubs++;
            remainingGenies--;
            return genie;
        }
        if (rubs % 2 != 0 && remainingGenies > 0) {
            Genie genie = new GrumpyGenie(maxWishes);
            System.out.println("I'm the Grumpy Genie");
            System.out.println("========================================");
            rubs++;
            remainingGenies--;
            return genie;
        }

            Genie demon = new Demon(maxWishes);
            System.out.println("I'm the Demon");
        System.out.println("========================================");
            return demon;
        }


    public void rechargeLamp(Genie demon) {
            demon.recycleDemon();
            rubs = 0;
            remainingGenies = maxGeniesCapacity;
            recharges++;
            System.out.println("You've recharged the magic lamp!");
        System.out.println("========================================");
        }

        public void compareLamps(MagicLamp otherLamp) {
            System.out.println("[Your lamp] max capacity: " + maxGeniesCapacity + "\n[Other lamp] max capacity: "
                    + otherLamp.getMaxGeniesCapacity());
            System.out.println("[Your lamp] remaining genies: " + remainingGenies +
                    "\n[Other lamp] remaining genies: " + otherLamp.getRemainingGenies());
            System.out.println("[Your lamp] recharges: " + recharges +
                    "\n[Other lamp] recharges: " + otherLamp.recharges);
            System.out.println("========================================");
        }
}

