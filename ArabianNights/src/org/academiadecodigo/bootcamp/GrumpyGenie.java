package org.academiadecodigo.bootcamp;

public class GrumpyGenie extends Genie {

    public GrumpyGenie(int maxWishes) {
        super(maxWishes, false);
    }

    @Override
    public void grantWishes() {
        if (getMaxWishes() == getRemainingWishes()) {
            super.grantWishes();
            System.out.println("You have no more wishes!");
            System.out.println("========================================");
            return;
        }
    }
}
