package org.academiadecodigo.bootcamp;

import java.util.Objects;

public class ArabianNights {

    public static void main(String[] args) {

        MagicLamp lampOne = new MagicLamp(3, 3);

        Genie genieOne = lampOne.rubLamp();
        genieOne.grantWishes();
        genieOne.grantWishes();
        genieOne.grantWishes();
        genieOne.grantWishes();
        genieOne.grantWishes();
        Genie genieTwo = lampOne.rubLamp();
        genieTwo.grantWishes();
        genieTwo.grantWishes();
        genieTwo.grantWishes();
        genieTwo.grantWishes();
        Genie genieThree = lampOne.rubLamp();
        genieThree.grantWishes();
        genieThree.grantWishes();
        genieThree.grantWishes();
        genieThree.grantWishes();
        genieThree.recycleDemon();
        Genie genieFour = lampOne.rubLamp();
        genieFour.grantWishes();
        lampOne.rechargeLamp(genieFour);
        

        MagicLamp lampTwo = new MagicLamp(5, 2);
        Genie genieOneLampTwo = lampTwo.rubLamp();
        genieOneLampTwo.grantWishes();
        genieOneLampTwo.grantWishes();
        genieOneLampTwo.grantWishes();
        genieOneLampTwo.grantWishes();
        genieOneLampTwo.grantWishes();
        Genie genieTwoLampTwo = lampTwo.rubLamp();
        genieTwoLampTwo.grantWishes();
        genieTwoLampTwo.grantWishes();
        genieTwoLampTwo.grantWishes();
        genieTwoLampTwo.grantWishes();
        Genie genieThreeLampTwo = lampTwo.rubLamp();
        genieThreeLampTwo.grantWishes();
        genieThreeLampTwo.grantWishes();
        genieThreeLampTwo.grantWishes();
        genieThreeLampTwo.grantWishes();



        lampOne.compareLamps(lampTwo);
    }

}
