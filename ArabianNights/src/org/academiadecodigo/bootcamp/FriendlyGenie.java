package org.academiadecodigo.bootcamp;

public class FriendlyGenie extends Genie {

    public FriendlyGenie(int maxWishes) {
        super(maxWishes, false);
    }

    @Override
    public void grantWishes() {
        if (getRemainingWishes() > 0) {
            System.out.println("You still have " + getRemainingWishes() + " wishes");
            System.out.println("========================================");
            super.grantWishes();

            if (getRemainingWishes() == 0) {
                System.out.println("You have no more wishes!");
                System.out.println("========================================");
                return;
            }
        }
    }
}
