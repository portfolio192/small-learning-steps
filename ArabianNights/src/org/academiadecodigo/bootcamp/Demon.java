package org.academiadecodigo.bootcamp;

public class Demon extends Genie {

    public Demon(int maxWishes) {
        super(maxWishes, true);
    }

    @Override
    public void grantWishes() {
        super.grantWishes();
    }
}
