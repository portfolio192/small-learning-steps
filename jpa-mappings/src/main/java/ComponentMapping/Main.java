package ComponentMapping;

public class Main {

    public static void main(String[] args) {

        WorkIt workIt = new WorkIt();

        Student student = new Student();
        Address address = new Address();

        address.setStreet("Rua 25 de Abril");
        address.setCity("Porto");
        address.setZipcode("4300");

        student.setId(1);
        student.setName("Ricardo");
        student.setAddress(address);

        workIt.save(student);
        //Student newStudent = workIt.getStudent(1);
        //workIt.remove(newStudent);
        workIt.close();

    }

}
