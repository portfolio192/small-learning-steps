package org.academiadecodigo.bootcamp;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;

public class UserTest {

    private EntityManagerFactory emf;

    public UserTest() {
        emf = Persistence.createEntityManagerFactory("batata");
    }

    public static void main(String[] args) {

        UserTest test = new UserTest();

        User user = new User();
        user.setName("Lourenço");
        user.setEmail("lou@ren.com");
        user.setSurname("Marques");

        test.save(user);
        User newUser = test.getUser(1);
        test.remove(newUser);
        test.close();

    }

    private User getUser(Integer id) {

        EntityManager em = emf.createEntityManager();

        try {
            return em.find(User.class, id);

        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private User save(User user) {

        EntityManager em = emf.createEntityManager();

        try {
            em.getTransaction().begin();
            User savedUser = em.merge(user);
            em.getTransaction().commit();
            return savedUser;

        } catch (RollbackException ex) {
            em.getTransaction().rollback();
            return null;

        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private void remove(User user) {

        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            User removeUser = em.find(User.class, user.getId());
            em.remove(removeUser);
            em.getTransaction().commit();

        } catch (RollbackException ex) {
            em.getTransaction().rollback();

        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void close() {
        emf.close();
    }

}
