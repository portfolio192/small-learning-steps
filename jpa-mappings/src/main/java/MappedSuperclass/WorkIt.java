package MappedSuperclass;

import ComponentMapping.Student;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;

public class WorkIt {

    private EntityManagerFactory entityFactory;

    public WorkIt() {
        entityFactory = Persistence.createEntityManagerFactory("batata");
    }

    public Student getStudent(Integer id) {

        EntityManager em = entityFactory.createEntityManager();

        try {
            return em.find(Student.class, id);

        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Student save(Student student) {

        EntityManager em = entityFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            Student savedStudent = em.merge(student);
            em.getTransaction().commit();
            System.out.println("test");
            return savedStudent;

        } catch (RollbackException ex) {
            em.getTransaction().rollback();
            return null;

        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void remove(Student student) {

        EntityManager em = entityFactory.createEntityManager();
        try {
            em.getTransaction().begin();
            Student removeStudent = em.find(Student.class, student.getId());
            em.remove(removeStudent);
            em.getTransaction().commit();

        } catch (RollbackException ex) {
            em.getTransaction().rollback();

        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void close() {
        entityFactory.close();
    }

}
