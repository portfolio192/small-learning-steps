package org.academiadecodigo.bootcamp;

public class Main {

    public static void main(String[] args) {

        Sound sound = new Sound();

        sound.play(Sound.SoundPaths.TODOCEGO);

    }
}
