package org.academiadecodigo.bootcamp;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class Sound implements LineListener {

    boolean playCompleted;

    void play(String audioFilePath) {
        File audioFile = new File(audioFilePath);

        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
            AudioFormat format = audioStream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            Clip audioClip = (Clip) AudioSystem.getLine(info);
            audioClip.addLineListener(this);
            audioClip.open(audioStream);
            audioClip.start();

            while (!playCompleted) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

            audioClip.close();
        } catch (UnsupportedAudioFileException ex) {
            System.out.println("The specified audio file is not supported.");
        } catch (LineUnavailableException ex) {
            System.out.println("Audio line for playing back is unavailable.");
        } catch (IOException ex) {
            System.out.println("Error playing the audio file.");
            ex.printStackTrace();
        }
    }

    @Override
    public void update(LineEvent event) {
        LineEvent.Type type = event.getType();

        if (type == LineEvent.Type.START) {
            System.out.println("Playback starded");

        } else if (type == LineEvent.Type.STOP) {
            playCompleted = true;
            System.out.println("Playback completed.");

        }
    }

    public static void play(SoundPaths sound) {
        Sound player = new Sound();
        player.play(sound.getFilepath());
    }

    public enum SoundPaths {
        TODOCEGO("Resources/EiTasTodoCego.wav"),
        ACERTASTE("Resources/Acertaste.wav"),
        FALHASTE("Resources/Falhaste.wav"),
        INDAHOUSE("Resources/InDaHouse.wav");

        private String filepath;

        public String getFilepath() {
            return filepath;
        }

        SoundPaths(String filepath){
            this.filepath = filepath;
        }



    }
}