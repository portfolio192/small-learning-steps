package org.academiadecodigo.bootcamp;

import java.util.Iterator;

public class Range implements Iterable<Integer> {

    private int min;
    private int max;
    private int currentPosition;
    private int currentPositionInverted;

    private boolean checkStatus;


    public boolean setCheckStatus(boolean checkStatus) {
        this.checkStatus = checkStatus;
        return checkStatus;
    }

    public Range(int min, int max, boolean checkStatus) {
        this.min = min;
        this.max = max;
        this.currentPosition = min - 1;
        this.currentPositionInverted = max + 1;
        this.checkStatus = checkStatus;
    }


    public Iterator<Integer> iterator() {
        if (checkStatus == true) {
            return new Iterator<Integer>() {

                @Override
                public boolean hasNext() {
                    if (currentPosition == max) {
                        //setCheckStatus(false);
                        return false;
                    }
                    return true;
                }

                @Override
                public Integer next() {
                    currentPosition++;
                    return currentPosition;
                }

                public void remove() {
                    next();
                }
            };
        } else if (checkStatus == false) {

            return new Iterator<Integer>() {

                @Override
                public boolean hasNext() {
                    if (currentPositionInverted == min) {
                        //setCheckStatus(true);
                        return false;
                    }
                    return true;
                }

                @Override
                public Integer next() {
                    currentPositionInverted--;
                    return currentPositionInverted;
                }

                public void remove() {
                    next();
                }
            };
        }
        return null;
    }
}