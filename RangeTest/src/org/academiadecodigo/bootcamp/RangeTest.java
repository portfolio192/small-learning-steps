package org.academiadecodigo.bootcamp;

import sun.lwawt.macosx.CSystemTray;

import java.util.Iterator;

public class RangeTest {

    public static void main(String[] args) {

        Range range = new Range(-5, 5, true);

        System.out.println("--- USING ITERATOR ---");
        Iterator<Integer> myRangeIterator = range.iterator();


        while (myRangeIterator.hasNext()) {

            int value = myRangeIterator.next();
            System.out.println(value);
        }
    }
}
