package org.academiadecodigo.bootcamp;

public class Main {

    public static void main(String[] args) {

        Telemovel meuTelemovel = new Telemovel("Samsung", "Android","Ricardo", 936666666, 6.2);

        Telemovel mulherTelemovel = new Telemovel("Xiaomi", "Android","Letícia", 939999999, 5.7);

        Telemovel filhaTelemovel = new Telemovel("Xiaomi", "Android", "Leonor", 931111111, 5.2);

        System.out.println(meuTelemovel);


    }
}
