package org.academiadecodigo.carcrash.field;

import org.academiadecodigo.carcrash.cars.Car;

public class Position {

    private int col = (int) Math.ceil(Math.random() * Field.getWidth());
    private int row = (int) Math.ceil(Math.random() * Field.getHeight());

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setRow(int row) {
        this.row = row;
    }


    public void moveUp() {
        this.setRow(this.getRow() -1);
        if (this.getRow() == 0) {
            this.setRow(this.getRow() +1);
        }
    }

    public void moveDown() {
        this.setRow(this.getRow() +1);
        if (this.getRow() == Field.getHeight()) {
            this.setRow(this.getRow() -1);
        }
    }

    public void moveLeft() {
        this.setCol(this.getCol() -1);
        if (this.getCol() == 0) {
            this.setCol(this.getCol() +1);
        }
    }

    public void moveRight() {
        this.setCol(this.getCol() +1);
        if (this.getCol() == Field.getWidth()) {
            this.setCol(this.getCol() -1);
        }
    }
}