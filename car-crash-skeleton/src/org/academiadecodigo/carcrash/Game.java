package org.academiadecodigo.carcrash;

import org.academiadecodigo.carcrash.cars.Car;
import org.academiadecodigo.carcrash.cars.CarFactory;
import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

public class Game {

    public static final int MANUFACTURED_CARS = 20;

    /**
     * Container of Cars
     */
    private Car[] cars;

    /**
     * Animation delay
     */
    private int delay;

    public Game(int cols, int rows, int delay) {

        Field.init(cols, rows);
        this.delay = delay;

    }

    /**
     * Creates a bunch of cars and randomly puts them in the field
     */
    public void init() {

        cars = new Car[MANUFACTURED_CARS];
        for (int i = 0; i < cars.length; i++) {
            cars[i] = CarFactory.getNewCar();
        }

        Field.draw(cars);

    }

    /**
     * Starts the animation
     *
     * @throws InterruptedException
     */
    public void start() throws InterruptedException {

        while (true) {

            // Pause for a while
            Thread.sleep(delay);

            // Move all cars
            moveAllCars();

            // Update screen
            Field.draw(cars);

        }
    }

    private void moveAllCars() {
<<<<<<< HEAD
        Car[] cars = this.cars;

        for (int i = 0; i < cars.length; i++) {
            int randomCars = ((int) Math.floor(Math.random() * 4));

            if (randomCars == 0 && !cars[i].isCrashed()) {
                cars[i].getPos().moveUp();
            }
            if (randomCars == 1 && !cars[i].isCrashed()) {
                cars[i].getPos().moveDown();
            }
            if (randomCars == 2 && !cars[i].isCrashed()) {
                cars[i].getPos().moveLeft();
            }
            if (randomCars == 3 && !cars[i].isCrashed()) {
                cars[i].getPos().moveRight();
            }
            for (int j = 0; j < cars.length; j++) {
                if (cars[i].getPos().getCol() == cars[j].getPos().getCol() &&
                    cars[i].getPos().getRow() == cars[j].getPos().getRow() &&
                    cars[i] != cars[j]){
                    cars[i].setCrashed(true);
                    cars[j].setCrashed(true);
                }
            }
=======
        for (int i = 0; i < cars.length; i++) {
            Position nextPosition = cars[i].Position();
            /*Position currPosition = cars[i].getPos();
            currPosition.setRow(currPosition.getCol() - 1);
            cars[i].setPos(currPosition);*/
>>>>>>> d5b55c076dda031b1ed8d0eeef704272248e2721
        }
    }
}
