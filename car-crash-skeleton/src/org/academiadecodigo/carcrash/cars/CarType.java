package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

public enum CarType {
    FIAT,
        MUSTANG;


    public static CarType giveMeCar() {
        return values()[(int) (Math.random() * values().length)];
    }
}
