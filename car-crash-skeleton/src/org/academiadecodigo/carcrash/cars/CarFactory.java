package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

public class CarFactory {


    public static Car getNewCar() {

        CarType type = CarType.giveMeCar();
        if (type == CarType.FIAT) {
            return new Fiat(new Position());
        }
        return new Mustang(new Position());
    }
}
