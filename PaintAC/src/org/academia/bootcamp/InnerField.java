package org.academia.bootcamp;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.LinkedList;

public class InnerField {
    Field field;
    private int cellSize = 30;
    public LinkedList<Rectangle> linkedList;

    public int getCellSize() {
        return cellSize;
    }

    public InnerField () {
        linkedList = new LinkedList();
    }

    public void populateInnerField() {
        field = new Field(1920,1080);
        for (int i = 0; i < field.getWidth(); i += cellSize) {
            for (int j = 0; j < field.getHeight(); j+= cellSize) {
                    linkedList.add(field.innerCells(i,j));
            }
        }
    }
}



