package org.academia.bootcamp;

import java.util.logging.Handler;

public class Builder {

    Field field;
    InnerField innerField;
    Cursor cursor;

    private Handler handler;

    public void totalGrid() {
        field = new Field(1920,1080);
        field.outsideGrid();
        innerField = new InnerField();
        innerField.populateInnerField();
        cursor = new Cursor();
        cursor.cursor();
    }
}
