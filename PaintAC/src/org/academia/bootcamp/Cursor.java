package org.academia.bootcamp;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class Cursor implements KeyboardHandler {

    private int width;
    private int height;
    private static int padding = 10;
    private int posX;
    private int posY;
    private Rectangle innerRectangle;
    private Rectangle cursorRectangle;

    private int cellSize = 15;
    InnerField innerField;

    public void cursor () {
        keyboardInit();
        this.posX = posX + padding;
        this.posY = posY + padding;
        cursorRectangle = new Rectangle(posX, posY, 30, 30);
        cursorRectangle.setColor(Color.BLUE);
        cursorRectangle.fill();
    }

    private void keyboardInit() {


        Keyboard keyboard = new Keyboard(this);

        //UP KEY
        KeyboardEvent upKeyPressed = new KeyboardEvent();
        upKeyPressed.setKey(KeyboardEvent.KEY_W);
        upKeyPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(upKeyPressed);

        //DOWN KEY
        KeyboardEvent downKeyPressed = new KeyboardEvent();
        downKeyPressed.setKey(KeyboardEvent.KEY_S);
        downKeyPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(downKeyPressed);


        //LEFT KEY
        KeyboardEvent leftKeyPressed = new KeyboardEvent();
        leftKeyPressed.setKey(KeyboardEvent.KEY_A);
        leftKeyPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(leftKeyPressed);

        //RIGHT KEY
        KeyboardEvent rightKeyPressed = new KeyboardEvent();
        rightKeyPressed.setKey(KeyboardEvent.KEY_D);
        rightKeyPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(rightKeyPressed);

        //PAINT KEY
        KeyboardEvent paintKeyPressed = new KeyboardEvent();
        paintKeyPressed.setKey(KeyboardEvent.KEY_SPACE);
        paintKeyPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(paintKeyPressed);

        keyboard.addEventListener(upKeyPressed);
        keyboard.addEventListener(downKeyPressed);
        keyboard.addEventListener(leftKeyPressed);
        keyboard.addEventListener(rightKeyPressed);
        keyboard.addEventListener(paintKeyPressed);
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_W) {
            innerRectangle.translate(0,-cellSize);
            posY -= cellSize;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_S) {
            innerRectangle.translate(0,cellSize);
            posY += cellSize;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_A) {
            innerRectangle.translate(-cellSize,0);
            posX -= cellSize;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_D) {
            innerRectangle.translate(cellSize,0);
            posX += cellSize;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_SPACE) {
            //rectangleColor = new Rectangle(posX,posY,30,30);
            for (int i = 0; i < innerField.linkedList.size(); i++) {
                if(innerField.linkedList(i) == cursorRectangle.getX() &&
                        innerField.linkedList.get(i) == cursorRectangle.getY()) {

                }
            }
            //rectangleColor.setColor(Color.BLUE);
            //rectangleColor.fill();
            //rectangleColor.isFilled();
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        }

}
