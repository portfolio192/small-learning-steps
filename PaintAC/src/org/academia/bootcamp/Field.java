package org.academia.bootcamp;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Field {

    private int width;
    private int height;
    private static int padding = 10;
    private int posX;
    private int posY;
    private Rectangle innerRectangle;

    Rectangle rectangle;


    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Field (int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void outsideGrid () {
        rectangle = new Rectangle(padding,padding, width,height);
        rectangle.draw();
    }

    public Rectangle innerCells(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
        innerRectangle = new Rectangle(posX + padding, posY + padding, 30, 30);
        innerRectangle.draw();
    return this.innerRectangle;
    }
}
