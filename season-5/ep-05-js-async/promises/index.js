// Put all js code here, avoid mixing js inside html template

// make sure the DOM is ready, window.onload does not provide 
// consistent behaviour across browsers
$(document).ready(function() {

    var api = 'https://api.github.com/search/users?type=Users&q=';

    // bind the event handler to the input box
    $('#search').change(function(event) {

        var search = event.target.value;

        if (!search) {
            return;
        }

        renderFetching(search);

        fetch(api + search) // ajax request to fetch github users
            .then(parseResponse) // async deserialize response json
            .then(getUsers) // extract useful info and sort by score
            .then(renderResults) // render the results on the screen 
            .catch(handleErrors); // generic error handler
    });

    function parseResponse(response) {

        if (!response.ok) {
            throw new Error('invalid response');
        } 

        return response.json();
    }

    function getUsers(data) {

        return data.items.map(function(user) {

            return {
                user: user.login,
                score: user.score,
                url: user.html_url
            };

        }).sort(function(a, b) {
            return b.score - a.score;
        });
    }

    function renderFetching(search) {
        $('#results').html('<p>Searching for ' + search + '...</p>');
    }

    function renderResults(results) {
        $('#results').html('<ol>' + renderListItems(results) + '</ol>');
    }

    function renderListItems(results) {
        return results.reduce(function(acc, curr) {
            return acc + '<li> - ' + '<b>' + curr.score +
                '</b>&nbsp<a href="' + curr.url + '">'+ curr.user + '</a></li>'
        }, '');
    }

    function handleErrors(error) {
        $('#results').html('<p style="color: red;">' + error + '</p>');
    }
});