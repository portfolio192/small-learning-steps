package org.academiadecodigo.bootcamp;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

public class PlayGround {

    private UserContainer users = new UserContainer();
    Prompt prompt = new Prompt(System.in, System.out);



    public void start() {
        login();
    }

    public void login() {
        StringInputScanner usernameQuestion = new StringInputScanner();
        usernameQuestion.setMessage("Insert your username:\n");
        String username = prompt.getUserInput(usernameQuestion);
        StringInputScanner passwordQuestion = new StringInputScanner();
        passwordQuestion.setMessage("Insert your password:\n");
        String password = prompt.getUserInput(passwordQuestion);

        check(username, password);
    }

    public boolean check(String username, String password) {
        for (User user : users.getUsers()) {
            if(username.equals(user.getUsername()) && password.equals(user.getPassword())) {
                System.out.println("Welcome " + username);
                return true;
            }
        }
        System.out.println("Wrong username and/or password ");
        return false;
    }
}
