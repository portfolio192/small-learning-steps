package org.academiadecodigo.bootcamp;
import java.util.LinkedList;

public class UserContainer {

    private LinkedList <User> users;



    public UserContainer () {
        users = new LinkedList<>();
        users.add(new User("Ricardo","academiadecodigo"));
        users.add(new User("Beatriz", "abeaélinda"));
        users.add(new User("Isabela","academiadecodigo"));
        users.add(new User("André", "academiadeocodigo"));
    }

    public LinkedList<User> getUsers() {
        return users;
    }
}
