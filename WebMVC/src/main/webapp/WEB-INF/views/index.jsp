<html>
<head>
    <style>

        .outer {
            display: flex;
            justify-content: center;

        }

        .head {
            width: 500px;
        }

        .table {
            background-color: cadetblue;
            border-radius: 6px;
            padding: 10px;
        }

        .user {
            align-content: space-around;
            color: orange;
        }

    </style>
</head>
<body>
<div class="outer">
    <div class="table">
    <div class="head">
        <h1>JAVABANK - Customer #1</h1>
            <div class="user">
                <h2>${user.name}</h2>
                <p>${user.email}</p>
                <p>${user.number}</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>
