package controller;

import model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MyServlet {

    @RequestMapping(method = RequestMethod.GET, value = "")
    public String sayHello(Model model) {

        User user = new User();
        model.addAttribute("user", user.createUser());

        return "index";
    }

}
