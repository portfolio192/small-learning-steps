package model;

public class User {

    String name;
    String email;
    String number;

    public User createUser() {
        User user = new User();
        user.setName("Ricardo Azevedo");
        user.setEmail("r.david.a@hotmail.com");
        user.setNumber("936857489");

        return user;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getNumber() {
        return number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
