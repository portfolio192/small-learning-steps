import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Main {
    public static void main(String[] args) {
       Rectangle rectangle = new Rectangle(10,10,400,400);
        rectangle.draw();
        Picture picture = new Picture(60,60,"resources/12.jpeg");
        picture.setColorAt(10,Color.BLUE);
        picture.draw();
    }
}
