package org.academiadecodigo.bootcamp.service;

import org.academiadecodigo.bootcamp.model.User;
import org.academiadecodigo.bootcamp.utils.Security;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class JdbcUserService implements UserService {

    private Connection dbConnection;
    private Statement statement;

    public JdbcUserService(Connection dbConnection) {
        this.dbConnection = dbConnection;
    }


    @Override
    public boolean authenticate(String username, String password) {

        try {

            String query = "SELECT username, password FROM user WHERE username=? AND password=?;";

            PreparedStatement statement = dbConnection.prepareStatement(query);
            statement.setString(1, username);
            statement.setString(2, Security.getHash(password));

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return true;
            }
            return false;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void add(User user) {

        try {

            String query = "INSERT INTO user (username, email, password, firstName, lastName, phone)"
                    + "VALUES (?, ?, ?, ?, ?, ?);";

            PreparedStatement statement = dbConnection.prepareStatement(query);
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getPhone());

            statement.executeUpdate();

            statement.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public User findByName(String username) {

        try {

            statement = dbConnection.createStatement();

            String query = "SELECT * FROM user WHERE username=?;";

            PreparedStatement statement = dbConnection.prepareStatement(query);
            statement.setString(1, username);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {

                String usernameValue = resultSet.getString("username");
                String emailValue = resultSet.getString("email");
                String passwordValue = resultSet.getString("password");
                String firstnameValue = resultSet.getString("firstName");
                String lastNameValue = resultSet.getString("lastName");
                String phoneValue = resultSet.getString("phone");

                return new User(usernameValue, emailValue, passwordValue, firstnameValue, lastNameValue, phoneValue);
            }

            resultSet.close();
            statement.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

            return null;
    }

    @Override
    public List<User> findAll() {
        try {

            List<User> list = new LinkedList<>();
            statement = dbConnection.createStatement();

            String query = "SELECT username FROM user;";

            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                String username = resultSet.getString("username");
                list.add(findByName(username));
            }

            resultSet.close();
            statement.close();

            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int count() {

        int result = 0;

        String query = "SELECT COUNT(*) FROM user;";


        try (ResultSet resultSet = statement.executeQuery(query)) {

            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }

            statement.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return result;
    }
}
