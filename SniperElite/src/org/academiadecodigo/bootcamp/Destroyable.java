package org.academiadecodigo.bootcamp;

public interface Destroyable {

    public abstract void hit(int damage);

    public abstract boolean isDestroyed();

}
