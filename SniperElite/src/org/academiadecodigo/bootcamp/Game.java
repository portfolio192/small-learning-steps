package org.academiadecodigo.bootcamp;

import sun.security.krb5.internal.crypto.Des;

import java.util.Arrays;

public class Game {

    private GameObject[] gameObjects;
    private SniperRifle sniperRifle;
    private int shotsFired;


    public GameObject[] getGameObjects() {
        return gameObjects;
    }

    public SniperRifle getSniperRifle() {
        return sniperRifle;
    }

    public int getShotsFired() {
        return shotsFired;
    }

    public Game(int number) {
        gameObjects = new GameObject[number];
    }

    public void start() {
        sniperRifle = new SniperRifle();
        createObjects();
        for (int i = 0; i < gameObjects.length; i++) {
            if (gameObjects[i] instanceof Destroyable) {
                while (!((Destroyable) gameObjects[i]).isDestroyed()) {
                    if (gameObjects[i].getMessage().equals("Enemy")) {
                        sniperRifle.shoot((Destroyable) gameObjects[i]);
                        shotsFired++;
                    }
                    if (gameObjects[i].getMessage().equals("Barrel")) {
                        sniperRifle.shoot((Destroyable) gameObjects[i]);
                        shotsFired++;
                    }
                }
            }
        }
        System.out.println("The amount of shots fired was " + shotsFired);
    }
    public GameObject[] createObjects() {
        for (int i = 0; i < gameObjects.length; i++) {
            int randomGenerator = (int) (Math.random() * 15);
            if (randomGenerator < 3) {
                gameObjects[i] = new Tree();
                System.out.println("New tree created");
            }
            if (randomGenerator >= 3 && randomGenerator < 7) {
                gameObjects[i] = new SoldierEnemy();
                System.out.println("Enemy soldier created");
            }
            if (randomGenerator >= 7 && randomGenerator < 12) {
                gameObjects[i] = new ArmouredEnemy();
                System.out.println("Enemy armoured soldier created");
            }
            if (randomGenerator >= 12){
                gameObjects[i] = new Barrel(BarrelType.giveMeBarrels());
                System.out.println("Barrel created");
            }
        }
        return gameObjects;
    }
}
