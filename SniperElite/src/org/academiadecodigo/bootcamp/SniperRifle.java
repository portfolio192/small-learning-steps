package org.academiadecodigo.bootcamp;

import sun.security.krb5.internal.crypto.Des;

public class SniperRifle {

    private int bulletDamage = 20;
    private final double HIT_PROB = 0.6;

    public int getBullet() {
        return bulletDamage;
    }

    public void shoot(Destroyable target) {
        if(Math.random() < HIT_PROB) {
            target.hit(bulletDamage);
            System.out.println("Direct hit");
        }
        else {
            System.out.println("Missed shot");
        }

    }
}
