package org.academiadecodigo.bootcamp;

abstract public class GameObject {

    abstract String getMessage();

}
