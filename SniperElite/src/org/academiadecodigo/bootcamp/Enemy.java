package org.academiadecodigo.bootcamp;

public class Enemy extends GameObject implements Destroyable {

    private int health;
    private boolean isDead;
    private boolean isDestroyed;

    public Enemy() {
        this.health = 100;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    @Override
    public boolean isDestroyed() {
        return isDestroyed;
    }

    public void setDestroyed(boolean destroyed) {
        isDestroyed = destroyed;
    }

    public void hit(int bulletDamage) {
        health -= bulletDamage;
        System.out.println("Your bullet damage was " + bulletDamage + " and remaining health " + health);
        if(health <= 0) {
            setDestroyed(true);
        }
    }

    @Override
    public String getMessage() {
        return "Enemy";
        }
}