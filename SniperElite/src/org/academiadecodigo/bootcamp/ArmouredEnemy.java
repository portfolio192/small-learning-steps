package org.academiadecodigo.bootcamp;

public class ArmouredEnemy extends Enemy {

    private int health;
    private int armour;

    public ArmouredEnemy() {
        this.armour = 50;
    }

    @Override
    public void hit(int bulletDamage) {
        super.hit(bulletDamage);
        if(armour > 0) {
        armour -= bulletDamage;
        }
        else {
            health -= bulletDamage;
        }
    }
}
