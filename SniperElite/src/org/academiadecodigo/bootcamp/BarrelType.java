package org.academiadecodigo.bootcamp;

    public enum BarrelType {
        PLASTIC(20),
        WOOD(40),
        METAL(80);

        private int maxDamage;

        BarrelType(int maxDamage) {
            this.maxDamage = maxDamage;

        }
        public int getMaxDamage() {
            return this.maxDamage;
        }

        public static BarrelType giveMeBarrels() {
            return values()[(int) (Math.random() * values().length)];
        }
    }