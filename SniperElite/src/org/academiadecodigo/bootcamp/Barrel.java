package org.academiadecodigo.bootcamp;

public class Barrel extends GameObject implements Destroyable {

    private BarrelType barrelType;
    private int currentDamage = 0;
    private boolean destroyed;

    public boolean isDestroyed() {
        return destroyed;
    }

    public void setDestroyed(boolean destroyed) {
        this.destroyed = destroyed;
    }

    public Barrel(BarrelType barrelType) {
        this.barrelType = barrelType;
    }

    public void hit(int bulletDamage) {
        currentDamage += bulletDamage;
        System.out.println("Your bullet damage is " + bulletDamage + " Remaining barrel durability is " + currentDamage + " It started has " + barrelType.getMaxDamage());
        if(currentDamage >= barrelType.getMaxDamage()) {
            setDestroyed(true);
        }
    }




    @Override
    public String getMessage() {
        return "Barrel";
    }
}
